<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181204111058 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_97A0ADA3BCF5E72D');
        $this->addSql('DROP INDEX IDX_97A0ADA3A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ticket AS SELECT id, user_id, categorie_id, titre, description, date FROM ticket');
        $this->addSql('DROP TABLE ticket');
        $this->addSql('CREATE TABLE ticket (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER DEFAULT NULL, categorie_id INTEGER NOT NULL, titre VARCHAR(255) NOT NULL COLLATE BINARY, description VARCHAR(500) NOT NULL COLLATE BINARY, date DATETIME NOT NULL, CONSTRAINT FK_97A0ADA3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_97A0ADA3BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO ticket (id, user_id, categorie_id, titre, description, date) SELECT id, user_id, categorie_id, titre, description, date FROM __temp__ticket');
        $this->addSql('DROP TABLE __temp__ticket');
        $this->addSql('CREATE INDEX IDX_97A0ADA3BCF5E72D ON ticket (categorie_id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA3A76ED395 ON ticket (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_97A0ADA3A76ED395');
        $this->addSql('DROP INDEX IDX_97A0ADA3BCF5E72D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ticket AS SELECT id, user_id, categorie_id, titre, description, date FROM ticket');
        $this->addSql('DROP TABLE ticket');
        $this->addSql('CREATE TABLE ticket (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER DEFAULT NULL, categorie_id INTEGER NOT NULL, titre VARCHAR(255) NOT NULL, description VARCHAR(500) NOT NULL, date DATETIME NOT NULL)');
        $this->addSql('INSERT INTO ticket (id, user_id, categorie_id, titre, description, date) SELECT id, user_id, categorie_id, titre, description, date FROM __temp__ticket');
        $this->addSql('DROP TABLE __temp__ticket');
        $this->addSql('CREATE INDEX IDX_97A0ADA3A76ED395 ON ticket (user_id)');
        $this->addSql('CREATE INDEX IDX_97A0ADA3BCF5E72D ON ticket (categorie_id)');
    }
}
