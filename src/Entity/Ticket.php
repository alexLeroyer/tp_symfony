<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 */
class Ticket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotNull
     * @Assert\Length(min="5", max="255")
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @Assert\NotNull
     * @Assert\Length(min="5", max="500")
     * @ORM\Column(type="string", length=500)
     */
    private $description;

    /**
     * @Assert\NotBlank
     * @Assert\DateTime
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @Assert\Valid
     * @Assert\Count(
     * min=1,
     * max=3,
     * minMessage = "Il doit y avoir au moins un utilisateur !",
     * maxMessage = "Trop d'utilisateurs spécifiés pour ce ticket !"
     * )
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tickets")
     */
    private $user;

    /**
     * @Assert\Valid
     * @Assert\NotNull
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="tickets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }
}
