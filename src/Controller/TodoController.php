<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Data\Todo;

class TodoController extends AbstractController
{
    /**
     * @Route("/todo", name="todo")
     */
    public function index()
    {
        return $this->render('todo/index.html.twig', [
            'controller_name' => 'TodoController',
        ]);
    }

    /**
     * @Route("/todo/todolist", name="todolist")
     */
    public function todo_list()
    {
        $todo = new Todo();
        return $this->render('todo/todolist.html.twig', [
            'list' => $todo->getAll(),
        ]);
    }
}
