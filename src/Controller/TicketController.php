<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Entity\Categorie;
use App\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tickets")
 */
class TicketController extends AbstractController{
    /**
     * @Route("/", name="ticket")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
 
        $entities = $em->getRepository(ticket::class)->findAll();
 
        return array('entities' => $entities);
    }
    }