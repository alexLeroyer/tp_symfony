<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DemoController extends AbstractController
{
    /**
     * @Route("/demo", name="demo")
     */
    public function index()
    {
        return $this->render('demo/index.html.twig', [
            'controller_name' => 'DemoController',
        ]);
    }
    /**
     * @Route("/demo/info", name="demo")
     */
    public function information()
    {
        return $this->render('demo/info.html.twig', [
            'title' => 'Information importante',
            'content' => 'description de mon information importante',
        ]);
    } 
}
