<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Entity\Categorie;
use App\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Faker;

class FakerController extends AbstractController
{
    /**
    * @Route("/faker", name="faker")
    **/
    public function createCategorie()
    {
      $faker = Faker\Factory::create('fr_FR');
      $em = $this->getDoctrine()->getManager();

      for($i = 0; $i <10; ++$i)
      {
        $category = new Categorie();
        $category->setNom($faker->word());
        $em->persist($category);
        dump($category);
      }
      $em->flush();

      return new Response('Categorie created');
    }

    public function createUser()
    {
      $faker = Faker\Factory::create('fr_FR');
      $em = $this->getDoctrine()->getManager();

      for($i = 0; $i <10; ++$i)
      {
        $user = new User();
        $user->setName($faker->word());
        $em->persist($user);
        dump($user);
      }
      $em->flush();

      return new Response('Categorie created');
    }

    public function createTicket()
    {
      $faker = Faker\Factory::create('fr_FR');
      $em = $this->getDoctrine()->getManager();

      $users = $this->getDoctrine()->getRepository(User::class)->findAll();
      $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

      for($i = 0; $i <10; ++$i)
      {
        $ticket = new Ticket();
        $ticket->setTitre($faker->word());

        $ticket->setCategorie($categories[randint(0,9)]);
      }
    }

    // public function testConstraint(ValidatorInterface $validator)
    // {
    //   $ticket = new Ticket();
    //   $category = new Categorie();


    //   for ($i=0; $i<=3; $i++)
    //   {
    //     $user = new User();
    //     $user->setNom('Lenom');
    //     $ticket->setUser($user);
    //   }

    //   $ticket->setCategorie($category);
    //   $ticket->setTitre('Le titre');


    // }
}
